#!/usr/bin/env bash

CC="cc65"
CCO="-O -Oi -Or -Os -T"
CCINC="-I ~/CODE/cc65/include"

AS="ca65"
ASO="-s"

LD="ld65"
LDO="-S 16384"

TARGET="--target apple2"
TARGETCPU="$TARGET --cpu 6502"
INCLUDE="-I ./include"

function ERROR_EXIT() {
	echo "ERROR!!! waiting for user...."
	read
	exit
}

function compile() {
	echo "compiling $1 ..."
	local ss="${1%.*}.s" && ss="${ss/src/build}"
	local so="${1%.*}.o" && so="${so/src/build}"
	local sd="${1%.*}.dep" && sd="${sd/src/build}"
	$CC $CCINC $CCO $TARGETCPU $INCLUDE --create-full-dep $sd $1 -o $ss
	if [ $? -eq 0 ]; then
		echo "assembling $ss ..."
		$AS $ASO $TARGETCPU $ss -o $so
		if [ $? -ne 0 ]; then
			ERROR_EXIT
		fi
	else
			ERROR_EXIT
	fi
}

function compile_all()
{
	local DIRS=(./src/test/ ./src/test/all ./src/test/all/hgr ./src/test/a2/gr ./src/test/a2/dgr ./src/test/a2/dhgr ./src/lib ./src/lib/draw ./src/lib/graphics ./src/lib/graphics/gr ./src/lib/graphics/dgr ./src/lib/graphics/hgr ./src/lib/graphics/hgr/bw ./src/lib/graphics/hgr/tv ./src/lib/graphics/hgr/colour ./src/lib/graphics/dhgr ./src/lib/graphics/dhgr/bw ./src/lib/graphics/dhgr/colour) 
	for d in "${DIRS[@]}"
	do
		echo "Processing $d"
		mkdir -p "${d/src/build}"
		local files="${d}/*.c"
		for f in $files
		do
			test -f "$f" || continue
			compile $f
		done
	done
}

function link()
{
	local binary_name=$1
	local object_file=$2
	local object_list=$3

	$LD $LDO -o ${binary_name} -m ${binary_name}.map $TARGET $object_file $object_list apple2.lib
	if [ $? -ne 0 ]; then
		ERROR_EXIT
	fi
}

function link_batch()
{
	local dirs=$1
	local suffix=$2
	local objs=$3
	
	for d in $dirs
	do
		echo "Building $d for $suffix"
		local dd=${d/build/bin}
		mkdir -p $dd
		for object in $d/*.o
		do
			test -f "$object" || continue
			name=${object%.*} && name=${name/build/bin}
		
			link ${name}_${suffix} $object "$objs"
		done	
	done			
}

function link_all()
{
	local all_objs="./build/test/common.o $(find './build/lib' -name '*.o')"
	local a2_objs=''
	local tk2k_objs=''
	for o in $all_objs
	do
		if [[ $o == *"_a2."* ]] ; then
			a2_objs="$a2_objs $o"
			continue
		fi
		if [[ $o == *"_tk2k."* ]] ; then
			tk2k_objs="$tk2k_objs $o"
			continue
		fi
		a2_objs="$a2_objs $o"
		tk2k_objs="$tk2k_objs $o"
	done
		
		
	shopt -s globstar
	
	for d in ./build/test/all/**
	do
		if [ -f "$d" ] ; then
			continue
		fi
		link_batch $d a2 "$a2_objs"
		link_batch $d tk2k "$tk2k_objs"
		done
	
	for d in ./build/test/a2/**
	do
		if [ -f "$d" ] ; then
			continue
		fi
		link_batch $d a2 "$a2_objs"
	done
	
	for d in ./build/test/tk2k/**
	do
		if [ -f "$d" ] ; then
			continue
		fi
		link_batch $d tk2k "$tk2k_objs"
	done
}

compile_all
link_all

echo "SUCCESS!! waiting for user...."
read
