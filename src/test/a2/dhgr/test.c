#include "graphics/dhgr.h"
#include "draw.h"

void common_test(graphics_ctx const * const ctx);

int main(int argc, char *argv[])
{
	graphics_ctx const * const ctx = dhgr();

	common_test(ctx);

	dhgr_close();

	return 0;
}
