#include "graphics/dgr.h"
#include "draw.h"

void common_test(graphics_ctx const * const ctx);

int main(int argc, char *argv[])
{
	graphics_ctx const * const ctx = dgr();

	common_test(ctx);

	dgr_close();

	return 0;
}
