#include "graphics/gr.h"
#include "draw.h"

void common_test(graphics_ctx const * const ctx);

int main(int argc, char *argv[])
{
	graphics_ctx const * const ctx = gr();

	common_test(ctx);

	gr_close();

	return 0;
}
