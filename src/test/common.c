/*
 * common.c
 *
 *  Created on: 26 de fev de 2017
 *      Author: lisias
 */

#include "graphics.h"
#include "draw.h"

#define MIN(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

extern colour_ndx const cores[16];

void common_test(graphics_ctx const * const ctx)
{
	x_coord const X_MAX = ctx->hw_desc->width - 1;
	y_coord const Y_MAX = ctx->hw_desc->height - 1;


	x_coord const X_LEFT 	= ctx->hw_desc->width * 20 / 100;
	y_coord const Y_TOP 	= ctx->hw_desc->height * 20 / 100;
	x_coord const X_RIGHT 	= ctx->hw_desc->width - X_LEFT;
	y_coord const Y_BOTTOM 	= ctx->hw_desc->height - Y_TOP;

	x_coord const X_MID = X_MAX / 2;
	y_coord const Y_MID = Y_MAX / 2;

	ctx->set_colour(ctx, cores[2]);
	draw_line(ctx, X_LEFT,Y_TOP, 	X_RIGHT,Y_TOP);
	draw_line(ctx, X_LEFT,Y_BOTTOM, X_RIGHT,Y_BOTTOM);
	draw_line(ctx, X_LEFT,Y_TOP, 	X_LEFT,Y_BOTTOM);
	draw_line(ctx, X_RIGHT,Y_TOP, 	X_RIGHT,Y_BOTTOM);
	draw_line(ctx, X_LEFT,Y_TOP, 	X_RIGHT,Y_BOTTOM);
	draw_line(ctx, X_RIGHT,Y_TOP, 	X_LEFT,Y_BOTTOM);

	ctx->set_colour(ctx, cores[3]);
	draw_circle(ctx, X_MID, Y_MID, 30, draw_line);
	draw_circle(ctx, X_MID, Y_MID, 60, draw_pixels);

	ctx->set_colour(ctx, cores[4]);
	draw_ellipse(ctx, X_LEFT, Y_TOP, MIN(X_LEFT, Y_TOP)/3, 2, 3, draw_pixels);
	draw_ellipse(ctx, X_RIGHT, Y_TOP, MIN(X_LEFT, Y_TOP)/3, 2, 3, draw_line);

	ctx->set_colour(ctx, cores[5]);
	draw_ellipse(ctx, X_MID, Y_TOP, MIN(X_LEFT, Y_TOP)/3, 1, 1, draw_line);
	ctx->set_colour(ctx, cores[4]);
	draw_ellipse(ctx, X_MID, Y_TOP, MIN(X_LEFT, Y_TOP)/3, 1, 1, draw_pixels);

	ctx->set_colour(ctx, cores[5]);
	draw_ellipse(ctx, X_LEFT, Y_BOTTOM, MIN(X_LEFT, Y_TOP)/3, 3, 2, draw_line);
	draw_ellipse(ctx, X_RIGHT, Y_BOTTOM, MIN(X_LEFT, Y_TOP)/3, 3, 2, draw_pixels);

	ctx->set_colour(ctx, cores[4]);
	draw_ellipse(ctx, X_MID, Y_BOTTOM, MIN(X_LEFT, Y_TOP)/3, 1, 1, draw_line);
	ctx->set_colour(ctx, cores[5]);
	draw_ellipse(ctx, X_MID, Y_BOTTOM, MIN(X_LEFT, Y_TOP)/3, 1, 1, draw_pixels);

	ctx->set_colour(ctx, cores[1]);
	ctx->put_pixel(ctx, X_MID,Y_MID);

	draw_pixels(ctx, 0,0, 				X_MAX,0);
	draw_pixels(ctx, X_MAX,Y_MAX, 		0,Y_MAX);
	draw_pixels(ctx, 0,Y_MID, 			X_MAX,Y_MID);
	draw_pixels(ctx, X_MID,0, 			X_MID,Y_MAX);

	draw_pixels(ctx, X_LEFT,Y_TOP, 		X_RIGHT,Y_TOP);
	draw_pixels(ctx, X_LEFT,Y_BOTTOM,	X_RIGHT,Y_BOTTOM);
	draw_pixels(ctx, X_LEFT,Y_MID, 		X_RIGHT,Y_MID);
	draw_pixels(ctx, X_MID,Y_TOP, 		X_MID,Y_BOTTOM);

	{
		int const XX_LEFT = X_LEFT / 2;
		int const XX_RIGHT = X_MAX - XX_LEFT;
		int const YY_TOP = Y_TOP / 2;
		int const YY_BOTTOM = Y_MAX - YY_TOP;

		ctx->set_colour(ctx, cores[6]);
		ctx->put_pixel(ctx, XX_LEFT, YY_TOP);
		draw_from_last_pixel(ctx, XX_RIGHT, YY_TOP);
		draw_from_last_pixel(ctx, XX_RIGHT, YY_BOTTOM);
		draw_from_last_pixel(ctx, XX_LEFT, YY_BOTTOM);
		draw_from_last_pixel(ctx, XX_LEFT, YY_TOP);
	}

}
