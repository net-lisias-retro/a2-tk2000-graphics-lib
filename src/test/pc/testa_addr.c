/*
 * testa_addr.c
 *
 *  Created on: 26 de fev de 2017
 *      Author: lisias
 */

#include<stdio.h>

#include "graphics/private.h"

int main(int argc, char *argv[])
{
	address const * const gr = graphics_lookuptable_gr(0x400, 24);
	address const * const hgr = graphics_lookuptable_hgr(0x2000, 192);

	for (int i = 0; i < 24; i++)
	{
		int const b = gr[i];
		printf("%i 	- %#X\n", i, b);
	}

	for (int i = 0; i < 192; i++)
	{
		int const b = hgr[i];
		printf("%i 	- %#X\n", i, b);
	}
}
