/*
 * test_draw_sdl.c
 *
 *  Created on: 2 de mar de 2017
 *      Author: lisias
 */

#include <stdbool.h>
#include <stdio.h>

#include <SDL/SDL.h>

#include "graphics/sdl.h"

void common_test(graphics_ctx const * const ctx);

int main(int argc, char * argv[])
{

	graphics_ctx const * const ctx = sdl();
	SDL_WM_SetCaption("DrawLib testing", "DrawLib testing");

	SDL_Event event;
	bool go = true;

	while (go) {
		while (SDL_PollEvent(&event)) {
			if (SDL_QUIT == event.type) {
				go = false;
			}
		}

		// fill the screen with black color
		SDL_FillRect(
				((SDL_Surface *)((graphics_sdl_ctx *)ctx)->screen),
				&((SDL_Surface *)((graphics_sdl_ctx *)ctx)->screen)->clip_rect,
				SDL_MapRGB(((graphics_sdl_ctx *)ctx)->screen->format, 0, 0, 0)
			);

		common_test(ctx);

		sdl_show(ctx);
	}

	return 0;
}
