#include "graphics/hgr.h"
#include "draw.h"

void common_test(graphics_ctx const * const ctx);

int main(int argc, char *argv[])
{
	graphics_ctx const * const ctx = hgr();

	common_test(ctx);

	hgr_close();

	return 0;
}
