#include "graphics/hgr.h"
#include "draw.h"

void common_test(graphics_ctx const * const ctx);

int main(int argc, char *argv[])
{
	graphics_ctx const * const ctx = hgr_graphics_init(0x2000);

	hgr_set_colour(ctx, 0);
	hgr_clear(ctx);
	hgr_show(1, false);

	common_test(ctx);

	hgr_graphics_deinit(ctx);

	return 0;
}
