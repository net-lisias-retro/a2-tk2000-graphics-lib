#include "graphics/hgr.h"
#include "draw.h"

void common_test(graphics_ctx const * const ctx);

int main(int argc, char *argv[])
{
	graphics_ctx const * const ctx = hgrtv_graphics_init(0x2000);

	hgrtv_set_colour(ctx, 0);
	hgrtv_clear(ctx);
	hgrtv_show(1, false);

	common_test(ctx);

	hgrtv_graphics_deinit(ctx);

	return 0;
}
