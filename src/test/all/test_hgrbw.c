#include "graphics/hgr.h"
#include "draw.h"

void common_test(graphics_ctx const * const ctx);

int main(int argc, char *argv[])
{
	graphics_ctx const * const ctx = hgrbw_graphics_init(0x2000);

	hgrbw_set_colour(ctx, 0);
	hgrbw_clear(ctx);
	hgrbw_show(1, false);

	common_test(ctx);

	hgrbw_graphics_deinit(ctx);

	return 0;
}
