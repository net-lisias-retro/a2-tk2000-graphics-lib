#include <assert.h>

#include "draw.h"

/**
 * draw_line_f compatible
 */
void draw_pixels(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, x_coord const x1, y_coord const y1)
{
	ctx->put_pixel(ctx, x0, y0);
	ctx->put_pixel(ctx, x1, y1);
}

/**
 * draw_line_f compatible
 */
void draw_line(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, x_coord const x1, y_coord const y1)
{
	if (x0 == x1) ctx->draw_vline(ctx, x0, y0, y1);
	else if (y0 == y1) ctx->draw_hline(ctx, x0, x1, y0);
	else draw_line_bresenham(ctx, x0,y0, x1,y1);
}

/**
 * draw_pixel_f compatible
 */
void draw_from_last_pixel(graphics_ctx const * const ctx, x_coord const x, y_coord const y)
{
	draw_line(ctx, ctx->last_x, ctx->last_y, x, y);
}
