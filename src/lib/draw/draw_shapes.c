#include <assert.h>

#include "draw.h"

void draw_circle(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, int const radius, draw_line_f* const f)
{
	x_coord x = radius;
	y_coord y = 0;
	int err = 0;

	while (x >= y)
	{
		{
			x_coord const xa = x0 + x;
			x_coord const xb = x0 - x;
			{
				register y_coord const y_ = y0 + y;
				f(ctx, xa, y_, xb, y_);
			}
			{
				register y_coord const y_ = y0 - y;
				f(ctx, xa, y_, xb, y_);
			}
		}
		{
			x_coord const xc = x0 + y;
			x_coord const xd = x0 - y;
			{
				register y_coord const y_ = y0 + x;
				f(ctx, xc, y_, xd, y_);
			}
			{
				register y_coord const y_ = y0 - x;
				f(ctx, xc, y_, xd, y_);
			}
		}

		if (err <= 0)
		{
			y += 1;
			err += 2*y + 1;
		}
		else
		{
			x -= 1;
			err -= 2*x + 1;
		}
	}
}

void draw_ellipse(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, int const radius, int xmult, int xdiv, draw_line_f* const f)
{
	int s = 1 - radius;
	int x = 0;
	int y = radius;

	xdiv = (xdiv < 1) ? 1 : xdiv;
	xmult = (xmult < 1) ? 1 : xmult;

	// Top Pixel
	ctx->put_pixel(ctx, x0, y0 + radius);

	{
		register int const xyaspect = (y * xmult) / xdiv;
		f(ctx, x0 + xyaspect, y0, x0 - xyaspect, y0);
	}

	{
		int ddfx = 1;
		int ddfy = -2 * radius;

		while (x < y) {
			if (s >= 0) {
				y--;
				ddfy += 2;
				s += ddfy;
			}
			x++;
			ddfx += 2;
			s += ddfx;

			{
				int const xxaspect = (x * xmult) / xdiv;
				{
					register int const xa = x0 + xxaspect;
					register int const xb = x0 - xxaspect;

					{
						register int const y_ = y0 + y;
						f(ctx, xa, y_, xb, y_);
					}
					{
						register int const y_ = y0 - y;
						f(ctx, xa, y_, xb, y_);
					}
				}
			}
			{
				int const xyaspect = (y * xmult) / xdiv;
				{
					register int const xa = x0 + xyaspect;
					register int const xb = x0 - xyaspect;

					{
						register int const y_ = y0 + x;
						f(ctx, xa, y_, xb, y_);
					}
					{
						register int const y_ = y0 - x;
						f(ctx, xa, y_, xb, y_);
					}
				}
			}

		}
	}

	// Bottom Pixel
	ctx->put_pixel(ctx, x0, y0 - radius);
}

void draw_box(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, int width, int height, draw_line_f* const f)
{
	x_coord const x1 = x0+width;
	int y = y0;
	ctx->draw_hline(ctx, x0, x1, y++);
	for (; y < height; y++)
		f(ctx, x0, y, x1, y);
	ctx->draw_hline(ctx, x0, x1, y0);
}
