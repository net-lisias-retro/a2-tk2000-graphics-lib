#include <assert.h>

#include "draw.h"


/**
 * draw_facet_f compatible
 */
void draw_3_dots(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, x_coord const x1, byte const y1, x_coord const x2, y_coord const y2)
{
	ctx->put_pixel(ctx, x0, y0);
	ctx->put_pixel(ctx, x1, y1);
	ctx->put_pixel(ctx, x2, y2);
}

/**
 * draw_facet_f compatible
 */
void draw_triangle(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, x_coord const x1, y_coord const y1, x_coord const x2, y_coord const y2)
{
	draw_line(ctx, x0, y0, x1, y1);
	draw_line(ctx, x1, y1, x2, y2);
	draw_line(ctx, x2, y2, x0, y0);
}

/**
 * draw_facet_f compatible
 */
void draw_facet(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, x_coord const x1, y_coord const y1, x_coord const x2, y_coord const y2)
{
	// TODO
}

void draw_facets(graphics_ctx const * const ctx, xy_coord const * const facets, size_t const size, draw_facet_f* const f)
{
	int i;
	for (i = 0; i < size; i += 3)
		f(ctx, facets[i].x, facets[i].y, facets[i+1].x, facets[i+1].y, facets[i+2].x, facets[i+2].y);
}

