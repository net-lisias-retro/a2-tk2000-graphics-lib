#include <assert.h>

#include "draw.h"

/**
 * draw_line_f compatible
 */
void draw_line_bresenham(graphics_ctx const * const ctx, x_coord x0, y_coord y0, x_coord const x1, y_coord const y1)
{
	int dx, dy, sx, sy;

	if (x0 < x1) {
		dx = x1 - x0;
		sx = 1;
	} else {
		dx = x0 - x1;
		sx = -1;
	}

	if (y0 < y1) {
		dy = y1 - y0;
		sy = 1;
	} else {
		dy = y0 - y1;
		sy = -1;
	}

	{
		int err = dx - dy;
		for (;;) {
			ctx->put_pixel(ctx, x0, y0);

			if (x0 == x1 && y0 == y1)
				break;

			{
				int const err2 = err * 2;

				if (err2 > (0 - dy)) {
					err -= dy;
					x0 += sx;
				}

				if (err2 < dx) {
					err += dx;
					y0 += sy;
				}
			}
		}
	}
}

void draw_lines(graphics_ctx const * const ctx, xy_coord const * const dots, size_t const size, put_pixel_f* const f)
{
	xy_coord const * p = dots;
	ctx->put_pixel(ctx, p[0].x, p[0].y);

	{
		size_t i;
		for (i = 1; i < size; i++)
			f(ctx, p[i].x, p[i].y);
	}
}

void draw_polygon(graphics_ctx const * const ctx, xy_coord const * const dots, size_t const size, put_pixel_f* const f)
{
	draw_lines(ctx, dots, size, f);
	f(ctx, ctx->last_x, ctx->last_y);
}
