#include <assert.h>

#include "graphics/hgr.h"

extern graphics_ctx const * hgr_ctx;

graphics_ctx const * const hgr2(void)
{
	assert(NULL == hgr_ctx);

	hgr_ctx = hgr_graphics_init(0xA000);
	hgr_set_colour(hgr_ctx, 0);
	hgr_clear(hgr_ctx);
	hgr_show(2, false);
	return hgr_ctx;
}
