#include <assert.h>
#include <string.h>

#include "graphics/hgr.h"

#define WIDTH 140
#define HEIGHT 192
#define COLOR_DEPTH 8
#define LINE_SIZE 40
#define FRAMEBUFFER_SIZE 0x2000

hardware_desc const HGRTV_DESCRIPTION = { WIDTH, HEIGHT, COLOR_DEPTH, LINE_SIZE, FRAMEBUFFER_SIZE };

void hgrtv_set_colour(graphics_ctx const * const ctx_, colour_ndx colour_num)
{
	CTX(graphics_hgr_ctx);

	// * Color masks for odd/even bytes, colors 0-7.
	static colour_mask const colours_even[]=  {0x00, 0x2A, 0x55, 0x7F, 0x80, 0xAA, 0xD5, 0xFF};
	static colour_mask const colours_odd[] =  {0x00, 0x55, 0x2A, 0x7F, 0x80, 0xD5, 0xAA, 0xFF};

	// * XOR mask for colors 0-7 - non-BW flip on odd/even.
	static colour_mask const colours_xor[] =  {0x00, 0x7F, 0x7F, 0x00, 0x00, 0x7F, 0x7F, 0x00};

	// * AND mask for the 7 pixel positions, high bit set
	static colour_mask const colours_and[] =  {0x81, 0x82, 0x84, 0x88, 0x90, 0xA0, 0xC0, 0xFF};

	assert(HGRTV == ctx_->safeguard);

	colour_num %= 8;

	*((colour_ndx*)&ctx->colour) 		= colour_num;
	*((colour_ndx*)&ctx->colour_even)	= colours_even[colour_num];
	*((colour_ndx*)&ctx->colour_odd)	= colours_odd[colour_num];
	*((colour_ndx*)&ctx->colour_xor)	= colours_xor[colour_num];
	*((colour_ndx*)&ctx->pixel_and)		= colours_and[colour_num];
}

void hgrtv_clear(graphics_ctx const * const ctx_)
{
	CTX(graphics_hgr_ctx);

	assert(HGRTV == ctx_->safeguard);

	memset((void*)ctx->lines[0], ctx->pixel_and, FRAMEBUFFER_SIZE);
}

void hgrtv_clear_safe(graphics_ctx const * const ctx_)
{
	CTX(graphics_hgr_ctx);

	assert(HGRTV == ctx_->safeguard);

	{
		int y;
		for (y = 0; y < HEIGHT; y++)
			memset((void*)ctx->lines[y], ctx->pixel_and, LINE_SIZE);
	}
}

void hgrtv_put_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_hgr_ctx);

	assert(HGRTV == ctx_->safeguard);

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y;

	// TODO
}

colour_mask hgrtv_get_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_hgr_ctx);

	assert(HGRTV == ctx_->safeguard);

	// TODO

	return 0;
}

void hgrtv_draw_vline(graphics_ctx const * const ctx_, x_coord const x, y_coord y0, y_coord y1)
{
	CTX(graphics_hgr_ctx);

	assert(HGRTV == ctx_->safeguard);

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y1;

	// TODO
}

void hgrtv_draw_hline(graphics_ctx const * const ctx_, x_coord x0, x_coord x1, y_coord const y)
{
	CTX(graphics_hgr_ctx);

	assert(HGRTV == ctx_->safeguard);

	*((x_coord*)&ctx->last_x) = x1;
	*((y_coord*)&ctx->last_y) = y;

	// TODO
}

