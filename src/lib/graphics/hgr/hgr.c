#include <assert.h>

#include "graphics/hgr.h"

graphics_ctx const * hgr_ctx = NULL;

graphics_ctx const * const hgr(void)
{
	assert(NULL == hgr_ctx);

	hgr_ctx = hgr_graphics_init(0x2000);
	hgr_set_colour(hgr_ctx, 0);
	hgr_clear(hgr_ctx);
	hgr_show(1, true);
	return hgr_ctx;
}

void hgr_close(void)
{
	assert(NULL != hgr_ctx);

	hgr_graphics_deinit(hgr_ctx);
	hgr_ctx = NULL;
}
