#include <assert.h>

#include "graphics/private.h"
#include "graphics/hgr.h"

extern hardware_desc const HGR_DESCRIPTION;

graphics_ctx* hgr_graphics_init(address const framebuffer)
{
	graphics_hgr_ctx * ctx = malloc(sizeof(graphics_hgr_ctx));

	assert(0 == (0x00ff & framebuffer));

	*((video_modes*)&ctx->safeguard) = HGR;

	*((y_coord*)&ctx->last_y) = *((x_coord*)&ctx->last_x) = 0;
	*((address const**)&ctx->lines) = graphics_lookuptable_hgr(framebuffer, HGR_DESCRIPTION.height);

	*((put_pixel_f **)&ctx->put_pixel) = hgr_put_pixel;
	*((get_pixel_f **)&ctx->get_pixel) = hgr_get_pixel;
	*((draw_vline_f **)&ctx->draw_vline) = hgr_draw_vline;
	*((draw_hline_f **)&ctx->draw_hline) = hgr_draw_hline;

	*((hardware_desc const **)&ctx->hw_desc) = &HGR_DESCRIPTION;

	return (graphics_ctx*)ctx;
}

void hgr_graphics_deinit(graphics_ctx const * const ctx_)
{
	CTX(graphics_hgr_ctx);

	assert(HGR == ctx_->safeguard);

	free((void*)ctx->lines);
	free((void*)ctx);
}

