#include <assert.h>

#include "graphics/private.h"
#include "graphics/hgr.h"

extern hardware_desc const HGRBW_DESCRIPTION;

graphics_ctx* hgrbw_graphics_init(address const framebuffer)
{
	graphics_hgr_ctx * ctx = malloc(sizeof(graphics_hgr_ctx));

	assert(0 == (0x00ff & framebuffer));

	*((video_modes*)&ctx->safeguard) = HGRBW;

	*((y_coord*)&ctx->last_y) = *((x_coord*)&ctx->last_x) = 0;
	*((address const**)&ctx->lines) = graphics_lookuptable_hgr(framebuffer, HGRBW_DESCRIPTION.height);

	*((put_pixel_f **)&ctx->put_pixel) = hgrbw_put_pixel;
	*((get_pixel_f **)&ctx->get_pixel) = hgrbw_get_pixel;
	*((draw_vline_f **)&ctx->draw_vline) = hgrbw_draw_vline;
	*((draw_hline_f **)&ctx->draw_hline) = hgrbw_draw_hline;

	*((hardware_desc const **)&ctx->hw_desc) = &HGRBW_DESCRIPTION;

	return (graphics_ctx*)ctx;
}

void hgrbw_graphics_deinit(graphics_ctx const * const ctx_)
{
	CTX(graphics_hgr_ctx);

	assert(HGRBW == ctx_->safeguard);

	free((void*)ctx->lines);
	free((void*)ctx);
}

