#include <assert.h>
#include <string.h>

#include "graphics/hgr.h"

#define WIDTH 280
#define HEIGHT 192
#define COLOR_DEPTH 2
#define LINE_SIZE 40
#define FRAMEBUFFER_SIZE 0x2000

hardware_desc const HGRBW_DESCRIPTION = { WIDTH, HEIGHT, COLOR_DEPTH, LINE_SIZE, FRAMEBUFFER_SIZE };

void hgrbw_set_colour(graphics_ctx const * const ctx_, colour_ndx colour_num)
{
	CTX(graphics_hgr_ctx);

	assert(HGRBW == ctx_->safeguard);

	colour_num %= 2;
	*((colour_ndx*)&ctx->colour) = colour_num;
	*((colour_mask*)&ctx->pixel_and) = 0 == colour_num ? 0x00 : 0x7F ;
}

void hgrbw_clear(graphics_ctx const * const ctx_)
{
	CTX(graphics_hgr_ctx);

	assert(HGRBW == ctx_->safeguard);

	memset((void*)ctx->lines[0], ctx->pixel_and, FRAMEBUFFER_SIZE);
}

void hgrbw_clear_safe(graphics_ctx const * const ctx_)
{
	CTX(graphics_hgr_ctx);

	assert(HGRBW == ctx_->safeguard);

	{
		int y;
		for (y = 0; y < HEIGHT; y++)
			memset((void*)ctx->lines[y], ctx->pixel_and, LINE_SIZE);
	}
}

void hgrbw_put_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_hgr_ctx);

	assert(HGRBW == ctx_->safeguard);
 	assert (y < HEIGHT) ;
 	assert (x < WIDTH) ;

 	{
		byte * const line = (byte *)ctx->lines[y];
		int shift = x / 7;
		byte const bit = 1 << (x % 7);

		byte b = line[shift] & !bit;
		b |= ctx->pixel_and & bit;
		line[shift] = b;
 	}

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y;
}

colour_mask hgrbw_get_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_hgr_ctx);

	assert(HGRBW == ctx_->safeguard);
 	assert(y < HEIGHT);
 	assert(x < WIDTH);

 	{
		byte * const line = (byte *)ctx->lines[y];
		int const shift = x / 7;
		byte const bit = 1 << (x % 7);

		byte const b = line[shift] & bit;
		return b ? 1 : 0;
 	}
}

void hgrbw_draw_vline(graphics_ctx const * const ctx_, x_coord const x, y_coord y0, y_coord y1)
{
	CTX(graphics_hgr_ctx);

	assert(HGRBW == ctx_->safeguard);

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y1;

	if (y0 > y1) {
		y_coord t = y0;
		y0 = y1;
		y1 = t;
	}

 	{
		int shift = x / 7;
		byte const bit = 1 << (x % 7);

		{
			for (; y0 <= y1; y0++)
			{
				byte * const line = (byte *)ctx->lines[y0];
				byte b = line[shift] & !bit;
				b |= ctx->pixel_and & bit;
				line[shift] = b;
			}
		}
 	}

}

void hgrbw_draw_hline(graphics_ctx const * const ctx_, x_coord x0, x_coord x1, y_coord const y)
{
	CTX(graphics_hgr_ctx);

	assert(HGRBW == ctx_->safeguard);

	*((x_coord*)&ctx->last_x) = x1;
	*((y_coord*)&ctx->last_y) = y;

	if (x0 > x1) {
		x_coord t = x0;
		x0 = x1;
		x1 = t;
	}

	{
		size_t shift_0 = x0 / 7;
		size_t shift_1 = x1 / 7;

		{
			int b = x0 % 7;
			if (b)
			{
				byte * const line = (byte *)ctx->lines[y];
				colour_mask m = ctx->pixel_and << b;
				byte b = line[shift_0] & !m;
				b |= ctx->pixel_and & m;
				line[shift_0] = b;
				shift_0++;
			}
		}

		{
			int b = x1 % 7;
			if (b)
			{
				byte * const line = (byte *)ctx->lines[y];
				colour_mask m = ctx->pixel_and << b;
				byte b = line[shift_1] & !m;
				b |= ctx->pixel_and & m;
				line[shift_1] = b;
				shift_1--;
			}
		}

		{
			byte * const line = (byte *)ctx->lines[y];

			int  s;
			for (s = shift_0; s <= shift_1; s++)
			{
				line[shift_1] = ctx->pixel_and;
			}
		}
	}

}
