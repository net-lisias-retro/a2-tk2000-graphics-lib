#include <assert.h>
#include <string.h>

#include "graphics/dhgr.h"

#define WIDTH 560
#define HEIGHT 192
#define COLOR_DEPTH 2
#define LINE_SIZE 40
#define FRAMEBUFFER_SIZE 0x2000

hardware_desc const DHGRBW_DESCRIPTION = { WIDTH, HEIGHT, COLOR_DEPTH, LINE_SIZE, FRAMEBUFFER_SIZE };

void dhgrbw_set_colour(graphics_ctx const * const ctx_, colour_ndx colour_num)
{
	CTX(graphics_dhgr_ctx);

	assert(DHGRBW == ctx_->safeguard);

	colour_num %= 2;
	*((colour_ndx*)&ctx->colour) = colour_num;
	//*((colour_mask*)&ctx->pixel_and) = 0 == colour_num ? 0x00 : 0x7F ;
}

void dhgrbw_clear(graphics_ctx const * const ctx_)
{
	CTX(graphics_dhgr_ctx);

	assert(DHGRBW == ctx_->safeguard);

	//TODO: SET WRITE AUX MEM
	//memset((void*)ctx->lines[0], ctx->pixel_and, FRAMEBUFFER_SIZE);
	//TODO: SET WRITE MAIN MEM
	//memset((void*)ctx->lines[0], ctx->pixel_and, FRAMEBUFFER_SIZE);
}

void dhgrbw_clear_safe(graphics_ctx const * const ctx_)
{
	CTX(graphics_dhgr_ctx);

	assert(DHGRBW == ctx_->safeguard);

	{
		int y;
		for (y = 0; y < HEIGHT; y++)
		{
			//TODO: SET WRITE AUX MEM
			//memset((void*)ctx->lines[y], ctx->pixel_and, LINE_SIZE);
			//TODO: SET WRITE MAIN MEM
			//memset((void*)ctx->lines[y], ctx->pixel_and, LINE_SIZE);
		}
	}
}

void dhgrbw_put_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_dhgr_ctx);

	assert (y < HEIGHT) ;
 	assert (x < WIDTH) ;
	assert(DHGRBW == ctx_->safeguard);

	// TODO

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y;
}

colour_mask dhgrbw_get_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_dhgr_ctx);

 	assert(y < HEIGHT);
 	assert(x < WIDTH);
	assert(DHGRBW == ctx_->safeguard);

	// TODO

	return 0;
}

void dhgrbw_draw_vline(graphics_ctx const * const ctx_, x_coord const x, y_coord y0, y_coord y1)
{
	CTX(graphics_dhgr_ctx);

	assert(DHGRBW == ctx_->safeguard);

	// TODO

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y1;
}

void dhgrbw_draw_hline(graphics_ctx const * const ctx_, x_coord x0, x_coord x1, y_coord const y)
{
	CTX(graphics_dhgr_ctx);

	assert(DHGRBW == ctx_->safeguard);

	*((x_coord*)&ctx->last_x) = x1;
	*((y_coord*)&ctx->last_y) = y;

	// TODO
}
