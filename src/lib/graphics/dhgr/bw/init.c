#include <assert.h>

#include "graphics/private.h"
#include "graphics/dhgr.h"

extern hardware_desc const DHGRBW_DESCRIPTION;

graphics_ctx* dhgrbw_graphics_init(address const framebuffer)
{
	graphics_dhgr_ctx * ctx = malloc(sizeof(graphics_dhgr_ctx));

 	assert(0 == (0x00ff & framebuffer));

 	*((video_modes*)&ctx->safeguard) = DHGRBW;

	*((y_coord*)&ctx->last_y) = *((x_coord*)&ctx->last_x) = 0;
	*((address const**)&ctx->lines) = graphics_lookuptable_hgr(framebuffer, DHGRBW_DESCRIPTION.height);

	*((put_pixel_f **)&ctx->put_pixel) = dhgrbw_put_pixel;
	*((get_pixel_f **)&ctx->get_pixel) = dhgrbw_get_pixel;
	*((draw_vline_f **)&ctx->draw_vline) = dhgrbw_draw_vline;
	*((draw_hline_f **)&ctx->draw_hline) = dhgrbw_draw_hline;

	*((hardware_desc const **)&ctx->hw_desc) = &DHGRBW_DESCRIPTION;

	return (graphics_ctx*)ctx;
}

void dhgrbw_graphics_deinit(graphics_ctx const * const ctx_)
{
	CTX(graphics_dhgr_ctx);

	assert(DHGRBW == ctx_->safeguard);

	free((void*)ctx->lines);
	free((void*)ctx);
}

void dhgrbw_show(byte const page, bool const text_window)
{
	// TODO
}

