#include <assert.h>

#include "graphics/dhgr.h"

static graphics_ctx const * ctx = NULL;

graphics_ctx const * const dhgr(void)
{
	assert(NULL == ctx);

	ctx = dhgr_graphics_init(0x2000);
	dhgr_set_colour(ctx, 0);
	dhgr_clear(ctx);
	dhgr_show(1, true);
	return ctx;
}

graphics_ctx const * const dhgr2(void)
{
	assert(NULL == ctx);

	ctx = dhgr_graphics_init(0x4000);
	dhgr_set_colour(ctx, 0);
	dhgr_clear(ctx);
	dhgr_show(2, false);
	return ctx;
}

void dhgr_close(void)
{
	assert(NULL != ctx);

	dhgr_graphics_deinit(ctx);
	ctx = NULL;
}
