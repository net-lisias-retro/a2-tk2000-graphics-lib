#include <assert.h>

#include "graphics/private.h"
#include "graphics/dhgr.h"

extern hardware_desc const DHGR_DESCRIPTION;

graphics_ctx* dhgr_graphics_init(address const framebuffer)
{
	graphics_dhgr_ctx * ctx = malloc(sizeof(graphics_dhgr_ctx));

 	assert(0 == (0x00ff & framebuffer));

	*((video_modes*)&ctx->safeguard) = DHGR;

	*((y_coord*)&ctx->last_y) = *((x_coord*)&ctx->last_x) = 0;
	*((address const**)&ctx->lines) = graphics_lookuptable_hgr(framebuffer, DHGR_DESCRIPTION.height);

	*((put_pixel_f **)&ctx->put_pixel) = dhgr_put_pixel;
	*((get_pixel_f **)&ctx->get_pixel) = dhgr_get_pixel;
	*((draw_vline_f **)&ctx->draw_vline) = dhgr_draw_vline;
	*((draw_hline_f **)&ctx->draw_hline) = dhgr_draw_hline;

	*((hardware_desc const **)&ctx->hw_desc) = &DHGR_DESCRIPTION;

	return (graphics_ctx*)ctx;
}

void dhgr_graphics_deinit(graphics_ctx const * const ctx_)
{
	CTX(graphics_dhgr_ctx);

	assert(DHGR == ctx_->safeguard);

	free((void*)ctx->lines);
	free((void*)ctx);
}

void dhgr_show(byte const page, bool const text_window)
{
	// TODO
}
