#include <assert.h>

#include "graphics/sdl.h"

#define WIDTH 640
#define HEIGHT 480
#define COLOR_DEPTH 8
#define LINE_SIZE (WIDTH * sizeof(colour_ndx))
#define FRAMEBUFFER_SIZE (HEIGHT * LINE_SIZE)

hardware_desc const SDL_DESCRIPTION = { WIDTH, HEIGHT, COLOR_DEPTH, LINE_SIZE, FRAMEBUFFER_SIZE };

void sdl_set_colour(graphics_ctx const * const ctx_, colour_ndx colour_num)
{
	CTX(graphics_sdl_ctx);

	assert(-1 == ctx_->safeguard);

	colour_num %= 256;

	*((colour_ndx*)&ctx->colour) 		= colour_num;
}

void sdl_clear(graphics_ctx const * const ctx_)
{
	CTX(graphics_sdl_ctx);

	assert(-1 == ctx_->safeguard);

	memset((void*)ctx->lines[0], ctx->colour, FRAMEBUFFER_SIZE);
}

void sdl_clear_safe(graphics_ctx const * const ctx_)
{
	CTX(graphics_sdl_ctx);

	assert(-1 == ctx_->safeguard);

	{
		int y;
		for (y = 0; y < HEIGHT; y++)
			memset((void*)ctx->lines[y], ctx->colour, LINE_SIZE);
	}
}

void sdl_put_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_sdl_ctx);

	assert(-1 == ctx_->safeguard);
 	assert (y < HEIGHT) ;
 	assert (x < WIDTH) ;

 	{
		byte * line = (byte *)ctx->lines[y];
		line[x] = ctx->colour;
 	}

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y;
}

colour_mask sdl_get_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_sdl_ctx);

	assert(-1 == ctx_->safeguard);
 	assert (y < HEIGHT) ;
 	assert (x < WIDTH) ;

 	{
		byte * line = (byte *)ctx->lines[y];
		return line[x];
 	}
}

void sdl_draw_vline(graphics_ctx const * const ctx_, x_coord const x, y_coord y0, y_coord y1)
{
	CTX(graphics_sdl_ctx);

	assert(-1 == ctx_->safeguard);
 	assert (y0 < HEIGHT) ;
 	assert (y1 < HEIGHT) ;
 	assert (x < WIDTH) ;

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y1;

	if (y0 > y1) {
		y_coord t = y0;
		y0 = y1;
		y1 = t;
	}

	{
		for (; y0 <= y1; y0++)
		{
			byte * const line = (byte *)ctx->lines[y0];
			line[x] = ctx->colour;
		}
	}

}

void sdl_draw_hline(graphics_ctx const * const ctx_, x_coord x0, x_coord x1, y_coord const y)
{
	CTX(graphics_sdl_ctx);

	assert(-1 == ctx_->safeguard);
 	assert (y < HEIGHT) ;
 	assert (x0 < WIDTH) ;
 	assert (x1 < WIDTH) ;

	*((x_coord*)&ctx->last_x) = x1;
	*((y_coord*)&ctx->last_y) = y;

	if (x0 > x1) {
		x_coord t = x0;
		x0 = x1;
		x1 = t;
	}

	{
		byte * const line = (byte *)ctx->lines[y];

		int  s;
		for (s = x0; s <= x1; s++)
		{
			line[s] = ctx->colour;
		}
	}
}

