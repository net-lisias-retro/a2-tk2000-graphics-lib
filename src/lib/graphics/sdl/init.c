#include <assert.h>

#include <SDL/SDL.h>

#include "graphics/private.h"
#include "graphics/sdl.h"

extern hardware_desc const SDL_DESCRIPTION;

static SDL_Color palette[256] = {{-1,-1,-1}};

static void generate_default_palette(void)
{
	for (unsigned int r = 0; r < 8; r++)
	{
		unsigned int const red = r * 255 / 7;
		for (unsigned int g = 0; g < 8; g++)
		{
			unsigned int const green = g * 255 / 7 ;
			for (unsigned int b = 0; b < 4; b++)
			{
				unsigned int const blue = b * 255 / 3 ;
				unsigned int i = (r << 5) | (g << 2) | b;
				palette[i].r = red;
				palette[i].g = green;
				palette[i].b = blue;
			}
		}
	}
}

graphics_ctx* sdl_graphics_init()
{
 	graphics_sdl_ctx * ctx = malloc(sizeof(graphics_sdl_ctx));

 	*((video_modes*)&ctx->safeguard) = -1;

 	*((SDL_Surface **)&ctx->screen) = SDL_SetVideoMode(SDL_DESCRIPTION.width, SDL_DESCRIPTION.height, 8, SDL_HWSURFACE | SDL_ASYNCBLIT | SDL_HWPALETTE);
    if (NULL == ctx->screen)
	{
		printf("Couldn't set screen mode to 640 x 480: %s\n", SDL_GetError());
		exit(1);
	}
    {
    	if (255 == palette[0].r) generate_default_palette();
    	if (1 != SDL_SetColors(ctx->screen, palette, 0, 256))
    	{
    		printf("Couldn't set palette : %s\n", SDL_GetError());
    		exit(1);
    	}
    }
    *((y_coord*)&ctx->last_y) = *((x_coord*)&ctx->last_x) = 0;
	*((address const**)&ctx->lines) = graphics_lookuptable_sdl(ctx->screen->pixels, SDL_DESCRIPTION.width, SDL_DESCRIPTION.height, 1);

	*((set_colour_f **)&ctx->set_colour) = sdl_set_colour;
	*((put_pixel_f **)&ctx->put_pixel) = sdl_put_pixel;
	*((get_pixel_f **)&ctx->get_pixel) = sdl_get_pixel;
	*((draw_vline_f **)&ctx->draw_vline) = sdl_draw_vline;
	*((draw_hline_f **)&ctx->draw_hline) = sdl_draw_hline;

	*((hardware_desc const **)&ctx->hw_desc) = &SDL_DESCRIPTION;

	return (graphics_ctx*)ctx;
}

void sdl_graphics_deinit(graphics_ctx const * const ctx_)
{
	CTX(graphics_sdl_ctx);

	assert(-1 == ctx->safeguard);

	free((void*)ctx->lines);
	free((void*)ctx);
}

void sdl_show(graphics_ctx const * const ctx_)
{
	CTX(graphics_sdl_ctx);

	assert(-1 == ctx_->safeguard);

	SDL_Flip((SDL_Surface*)ctx->screen);
}

