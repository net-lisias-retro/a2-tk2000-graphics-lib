#include <assert.h>

#include <SDL/SDL.h>

#include "graphics/sdl.h"

graphics_ctx const * sdl_ctx = NULL;

graphics_ctx const * const sdl(void)
{
	assert(NULL == sdl_ctx);

	if (0 != SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER))
	{
		fprintf(stderr, "\nUnable to initialize SDL:  %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	sdl_ctx = sdl_graphics_init();
	sdl_set_colour(sdl_ctx, 0);
	sdl_clear(sdl_ctx);
	sdl_show(sdl_ctx);

	return sdl_ctx;
}

void sdl_close(void)
{
	assert(NULL != sdl_ctx);

	sdl_graphics_deinit(sdl_ctx);
	sdl_ctx = NULL;
}
