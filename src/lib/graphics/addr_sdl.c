/*
 * addr_sdl.c
 *
 *  Created on: 3 de mar de 2017
 *      Author: lisias
 */

#include "graphics/private.h"

address const * const graphics_lookuptable_sdl(void * framebuffer, x_coord const width, y_coord const height, colour_ndx const pixel_size)
{
	address * const b = malloc(height * sizeof(address));
	{
		for (int y = 0; y < height; y++)
			b[y] = framebuffer + y * (pixel_size * width);
	}
	return b;
}

