#include <assert.h>

#include "graphics/gr.h"

static graphics_ctx const * ctx = NULL;

graphics_ctx const * const gr(void)
{
	assert(NULL == ctx);

	ctx = gr_graphics_init(0x0400);
	gr_set_colour(ctx, 0);
	gr_clear(ctx);
	gr_show(1, true);
	return ctx;
}

graphics_ctx const * const gr2(void)
{
	assert(NULL == ctx);

	ctx = gr_graphics_init(0x0800);
	gr_set_colour(ctx, 0);
	gr_clear(ctx);
	gr_show(2, true);
	return ctx;
}

void gr_close(void)
{
	assert(NULL != ctx);

	gr_graphics_deinit(ctx);
	ctx = NULL;
}
