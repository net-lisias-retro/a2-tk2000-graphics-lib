#include <assert.h>

#include "graphics/private.h"
#include "graphics/gr.h"

extern hardware_desc const GR_DESCRIPTION;

graphics_ctx* gr_graphics_init(address const framebuffer)
{
	graphics_gr_ctx * ctx = malloc(sizeof(graphics_gr_ctx));

	assert(0 == (0x00ff & framebuffer));

	*((video_modes*)&ctx->safeguard) = GR;

	*((y_coord*)&ctx->last_y) = *((x_coord*)&ctx->last_x) = 0;
	*((address const**)&ctx->lines) = graphics_lookuptable_hgr(framebuffer, GR_DESCRIPTION.height);

	*((put_pixel_f **)&ctx->put_pixel) = gr_put_pixel;
	*((get_pixel_f **)&ctx->get_pixel) = gr_get_pixel;
	*((draw_vline_f **)&ctx->draw_vline) = gr_draw_vline;
	*((draw_hline_f **)&ctx->draw_hline) = gr_draw_hline;

	*((hardware_desc const **)&ctx->hw_desc) = &GR_DESCRIPTION;

	return (graphics_ctx*)ctx;
}

void gr_graphics_deinit(graphics_ctx const * const ctx_)
{
	CTX(graphics_gr_ctx);

	assert(GR == ctx_->safeguard);

	free((void*)ctx->lines);
	free((void*)ctx);
}

void gr_show(byte const page, bool const text_window)
{
	// TODO
}
