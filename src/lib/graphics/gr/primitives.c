#include <assert.h>

#include "graphics/gr.h"

#define WIDTH 40
#define HEIGHT 48
#define COLOR_DEPTH 16
#define LINE_SIZE 40
#define FRAMEBUFFER_SIZE 0x0400

hardware_desc const GR_DESCRIPTION = { WIDTH, HEIGHT, COLOR_DEPTH, LINE_SIZE, FRAMEBUFFER_SIZE };

void gr_set_colour(graphics_ctx const * const ctx_, colour_ndx colour_num)
{
	CTX(graphics_gr_ctx);

	assert(GR == ctx_->safeguard);

	colour_num %= 16;

	*((colour_ndx*)&ctx->colour) 		= colour_num;
}

void gr_clear(graphics_ctx const * const ctx_)
{
	CTX(graphics_gr_ctx);

	assert(GR == ctx_->safeguard);

	// TODO
}

void gr_clear_safe(graphics_ctx const * const ctx_)
{
	CTX(graphics_gr_ctx);

	assert(GR == ctx_->safeguard);

	// TODO
}

void gr_put_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_gr_ctx);

	assert(GR == ctx_->safeguard);

	// TODO

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y;
}

colour_mask gr_get_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_gr_ctx);

	assert(GR == ctx_->safeguard);

	// TODO

	return 0;
}

void gr_draw_vline(graphics_ctx const * const ctx_, x_coord const x, y_coord y0, y_coord y1)
{
	CTX(graphics_gr_ctx);

	assert(GR == ctx_->safeguard);

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y1;

	// TODO
}

void gr_draw_hline(graphics_ctx const * const ctx_, x_coord x0, x_coord x1, y_coord const y)
{
	CTX(graphics_gr_ctx);

	assert(GR == ctx_->safeguard);

	*((x_coord*)&ctx->last_x) = x1;
	*((y_coord*)&ctx->last_y) = y;

	// TODO
}

