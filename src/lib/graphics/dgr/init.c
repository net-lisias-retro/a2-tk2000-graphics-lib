#include <assert.h>

#include "graphics/private.h"
#include "graphics/dgr.h"

extern hardware_desc const DGR_DESCRIPTION;

graphics_ctx* dgr_graphics_init(address const framebuffer)
{
	graphics_dgr_ctx * ctx = malloc(sizeof(graphics_dgr_ctx));

 	assert(0 == (0x00ff & framebuffer));

 	*((video_modes*)&ctx->safeguard) = DGR;

	*((y_coord*)&ctx->last_y) = *((x_coord*)&ctx->last_x) = 0;
	*((address const**)&ctx->lines) = graphics_lookuptable_hgr(framebuffer, DGR_DESCRIPTION.height);

	*((put_pixel_f **)&ctx->put_pixel) = dgr_put_pixel;
	*((get_pixel_f **)&ctx->get_pixel) = dgr_get_pixel;
	*((draw_vline_f **)&ctx->draw_vline) = dgr_draw_vline;
	*((draw_hline_f **)&ctx->draw_hline) = dgr_draw_hline;

	*((hardware_desc const **)&ctx->hw_desc) = &DGR_DESCRIPTION;

	return (graphics_ctx*)ctx;
}

void dgr_graphics_deinit(graphics_ctx const * const ctx_)
{
	CTX(graphics_dgr_ctx);

	assert(DGR == ctx_->safeguard);

	free((void*)ctx->lines);
	free((void*)ctx);
}

void dgr_show(byte const page, bool const text_window)
{
	// TODO
}
