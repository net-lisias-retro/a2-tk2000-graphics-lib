#include <assert.h>

#include "graphics/dgr.h"

static graphics_ctx const * ctx = NULL;

graphics_ctx const * const dgr(void)
{
	assert(NULL == ctx);

	ctx = dgr_graphics_init(0x0400);
	dgr_set_colour(ctx, 0);
	dgr_clear(ctx);
	dgr_show(1, true);
	return ctx;
}

graphics_ctx const * const dgr2(void)
{
	assert(NULL == ctx);

	ctx = dgr_graphics_init(0x0800);
	dgr_set_colour(ctx, 0);
	dgr_clear(ctx);
	dgr_show(2, true);
	return ctx;
}

void dgr_close(void)
{
	assert(NULL != ctx);

	dgr_graphics_deinit(ctx);
	ctx = NULL;
}
