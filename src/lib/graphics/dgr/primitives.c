#include <assert.h>

#include "graphics/dgr.h"

#define WIDTH 80
#define HEIGHT 48
#define COLOR_DEPTH 16
#define LINE_SIZE 40
#define FRAMEBUFFER_SIZE 0x0400

hardware_desc const DGR_DESCRIPTION = { WIDTH, HEIGHT, COLOR_DEPTH, LINE_SIZE, FRAMEBUFFER_SIZE };

void dgr_set_colour(graphics_ctx const * const ctx_, colour_ndx colour_num)
{
	// * Color masks for odd/even bytes, colors 0-15.
	static colour_mask const colours_even[]=  {0};
	static colour_mask const colours_odd[] =  {0};

	// * XOR mask for colors 0-7 - non-BW flip on odd/even.
	static colour_mask const colours_xor[] =  {0};

	// * AND mask for the 7 pixel positions, high bit set
	static colour_mask const colours_and[] =  {0};

	CTX(graphics_dgr_ctx);

	assert(DGR == ctx_->safeguard);

	colour_num %= 8;

	*((colour_ndx*)&ctx->colour) 		= colour_num;
	// TODO Colour Masks!
//	*((colour_ndx*)&ctx->colour_even)	= colours_even[colour_num];
//	*((colour_ndx*)&ctx->colour_odd)	= colours_odd[colour_num];
//	*((colour_ndx*)&ctx->colour_xor)	= colours_xor[colour_num];
//	*((colour_ndx*)&ctx->pixel_and)	= colours_and[colour_num];
}

void dgr_clear(graphics_ctx const * const ctx)
{
	// TODO
}

void dgr_clear_safe(graphics_ctx const * const ctx)
{
	// TODO
}

void dgr_put_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_dgr_ctx);

	assert(DGR == ctx_->safeguard);

	// TODO

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y;
}

colour_mask dgr_get_pixel(graphics_ctx const * const ctx_, x_coord const x, y_coord const y)
{
	CTX(graphics_dgr_ctx);

	assert(DGR == ctx_->safeguard);

	// TODO

	return 0;
}

void dgr_draw_vline(graphics_ctx const * const ctx_, x_coord const x, y_coord y0, y_coord y1)
{
	CTX(graphics_dgr_ctx);

	assert(DGR == ctx_->safeguard);

	*((x_coord*)&ctx->last_x) = x;
	*((y_coord*)&ctx->last_y) = y1;

	// TODO
}

void dgr_draw_hline(graphics_ctx const * const ctx_, x_coord x0, x_coord x1, y_coord const y)
{
	CTX(graphics_dgr_ctx);

	assert(DGR == ctx_->safeguard);

	*((x_coord*)&ctx->last_x) = x1;
	*((y_coord*)&ctx->last_y) = y;

	// TODO
}
