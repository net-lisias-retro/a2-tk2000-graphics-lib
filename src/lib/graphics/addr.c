/*
 * addr.c
 *
 *  Created on: 25 de fev de 2017
 *      Author: lisias
 */

#include "graphics/private.h"

address const * const graphics_lookuptable_hgr(address const framebuffer, y_coord const height)
{
	address * const b = malloc(height * sizeof(address));
	{
		int y;
		// https://github.com/AppleWin/AppleWin/wiki
		for (y = 0; y < height; y++)
			b[y] = framebuffer + (y&7)*0x400 + ((y/8)&7)*0x80 + (y/64)*0x28;
	}
	return b;
}

address const * const graphics_lookuptable_gr(address const framebuffer, y_coord const height)
{
	address * const b = malloc(height * sizeof(address));
	{
		int y;
		// https://github.com/AppleWin/AppleWin/wiki
		for (y = 0; y < height; y++)
			b[y] = framebuffer + (((y >> 1) & 3) << 8) + ( ((y << 7) & 0x80) | ((y & 0x18) << 2) | (y & 0x18) );
	}
	return b;
}
