/*
 * cpuPC.h
 *
 *  Created on: 3 de mar de 2017
 *      Author: lisias
 */

#ifndef INCLUDE_CPUPC_H_
#define INCLUDE_CPUPC_H_

#include <stddef.h>
#include <stdint.h>

typedef uint8_t byte;
typedef uint16_t word;
typedef void* address;

#endif /* INCLUDE_CPUPC_H_ */
