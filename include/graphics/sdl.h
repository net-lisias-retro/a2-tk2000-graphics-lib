/*
 * graphics/sdl.h
 *
 *  Created on: 3 de mar de 2017
 *      Author: lisias
 */

#ifndef INCLUDE_GRAPHICS_SDL_H_
#define INCLUDE_GRAPHICS_SDL_H_

#include <SDL/SDL.h>
#include "graphics.h"

typedef struct graphics_sdl_ctx_ {
	// Hack, but it works. I need a kind of inheritance, but don't know if cc65 supports it.
	// So, I Gone Horse here.
	// For future reference:
	//		http://www.deleveld.dds.nl/inherit.htm
	//		GCC and Anonymous Structs

	// Copy from graphics_ctx
		colour_ndx const colour;
		x_coord const last_x;
		y_coord const last_y;

		set_colour_f	* const	set_colour;
		put_pixel_f		* const	put_pixel;
		get_pixel_f		* const	get_pixel;
		draw_vline_f	* const	draw_vline;
		draw_hline_f	* const	draw_hline;

		hardware_desc	const * const 	hw_desc;

		video_modes 	const safeguard;
	// end of copy

	SDL_Surface     const * const	screen;
	address 		const * const	lines;
} graphics_sdl_ctx;

graphics_ctx const * const sdl(void);
void sdl_close(void);

/**
 * Mostra a p�gina selecionada, com op��o de janela de texto no A2.
 *
 * page : valores 1 ou 2
 * text_window : true para mostrar as 4 �ltimas linhas de texto, false para full graphics
 */
void sdl_show(graphics_ctx const * const ctx_);

/**
 * Inicializa um contexto gr�fico, com framebuffer iniciando no endere�o selecionado.
 *
 * O framebuffer precisa ser previamente alocado ou reservado.
 *
 * framebuffer_page : endere�o do framebuffer; Deve iniciar numa p�gina do 6502 (0x00FF & addr tem que dar zero).
 *
 * Retorna um contexto gr�fico.
 */
graphics_ctx* sdl_graphics_init();

/**
 * Deinicializa um contexto gr�fico, devolvendo qualquer mem�ria alocada.
 */
void sdl_graphics_deinit(graphics_ctx const * const ctx);

/**
 */
void sdl_set_colour(graphics_ctx const * const ctx, colour_ndx colour_num);

/**
 * Limpa a tela gr�fica com a cor corrente usando o m�todo mais r�oido.
 *
 * Memory holes ser�o destru�dos.
 */
void sdl_clear(graphics_ctx const * const ctx);

/**
 * Limpa a tela gr�fica com a cor corrente usando o m�todo seguro para os Memory Holes.
 *
 * A limpeza � feita em linhas seq�enciais, evitadno o "efeito persiana" inerente ao Apple 2.
 */
void sdl_clear_safe(graphics_ctx const * const ctx);

void sdl_put_pixel(graphics_ctx const * const ctx, x_coord const x, y_coord const y);

colour_mask sdl_get_pixel(graphics_ctx const * const ctx, x_coord const x, y_coord const y);

void sdl_draw_vline(graphics_ctx const * const ctx, x_coord const x, y_coord y0, y_coord y1);

void sdl_draw_hline(graphics_ctx const * const ctx, x_coord x0, x_coord x1, y_coord const y);

#endif /* INCLUDE_GRAPHICS_HGR_H_ */
