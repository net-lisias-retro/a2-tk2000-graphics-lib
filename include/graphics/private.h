/*
 * private.h
 *
 *  Created on: 25 de fev de 2017
 *      Author: lisias
 */

#ifndef INCLUDE_GRAPHICS_PRIVATE_H_
#define INCLUDE_GRAPHICS_PRIVATE_H_

#include "graphics.h"

address const * const graphics_lookuptable_gr(address const framebuffer, y_coord const height);
address const * const graphics_lookuptable_hgr(address const framebuffer, y_coord const height);

#ifdef _SDL_H
address const * const graphics_lookuptable_sdl(void * framebuffer, x_coord const width, y_coord const height, colour_ndx const pixel_size);
#endif

#endif /* INCLUDE_GRAPHICS_PRIVATE_H_ */
