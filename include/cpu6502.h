/*
 * cpu6502.h
 *
 *  Created on: 24 de fev de 2017
 *      Author: lisias
 */

#ifndef INCLUDE_CPU6502_H_
#define INCLUDE_CPU6502_H_

#include <stddef.h>
#include <stdint.h>

typedef uint8_t byte;
typedef uint16_t word;
typedef void* address;

#endif /* INCLUDE_CPU6502_H_ */
