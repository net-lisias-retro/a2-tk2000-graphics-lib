/*
 * graphics.h
 *
 *  Created on: 25 de fev de 2017
 *      Author: lisias
 */

#ifndef INCLUDE_GRAPHICS_H_
#define INCLUDE_GRAPHICS_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#ifdef CLIENT_PC
	#include "cpuPC.h"

	typedef byte colour_ndx;
	typedef byte colour_mask;
	typedef unsigned int x_coord;
	typedef unsigned int y_coord;
#else
	#include "cpu6502.h"

	typedef byte colour_ndx;
	typedef byte colour_mask;
	typedef word x_coord;
	typedef byte y_coord;
#endif

typedef struct hardware_desc_ {
	x_coord const width;
	y_coord const height;
	colour_ndx const color_depth;
	size_t const line_size;
	size_t const framebuffer_size;
} hardware_desc;

typedef struct xy_coord_ {
	x_coord x;
	y_coord y;
} xy_coord;

struct graphics_ctx_;

typedef void set_colour_f(struct graphics_ctx_ const * const ctx, colour_ndx const colour);
typedef void put_pixel_f(struct graphics_ctx_ const * const ctx, x_coord const x, y_coord const y);
typedef colour_mask get_pixel_f(struct graphics_ctx_ const * const ctx, x_coord const x, y_coord const y);
typedef void draw_vline_f(struct graphics_ctx_ const * const ctx, x_coord const x, y_coord y0, y_coord y1);
typedef void draw_hline_f(struct graphics_ctx_ const * const ctx, x_coord x0, x_coord x1, y_coord const y);
typedef void draw_line_f(struct graphics_ctx_ const * const ctx, x_coord const x0, y_coord const y0, x_coord const x1, y_coord const y1);
typedef void draw_facet_f(struct graphics_ctx_ const * const ctx, x_coord const x0, y_coord const y0, x_coord const x1, y_coord const y1, x_coord const x2, y_coord const y2);

typedef enum {GR, DGR, HGR, HGRBW, HGRTV, DHGR, DHGRBW} video_modes;

typedef struct graphics_ctx_ {
	colour_ndx const colour;
	x_coord const last_x;
	y_coord const last_y;

	set_colour_f	* const	set_colour;
	put_pixel_f		* const	put_pixel;
	get_pixel_f		* const	get_pixel;
	draw_vline_f	* const	draw_vline;
	draw_hline_f	* const	draw_hline;

	hardware_desc	const * const 	hw_desc;

	video_modes 	const safeguard;
} graphics_ctx;

#define CTX(type) type * const ctx = (type *)ctx_

#endif /* INCLUDE_GRAPHICS_H_ */
