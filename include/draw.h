/*
 * draw.h
 *
 *  Created on: 24 de fev de 2017
 *      Author: lisias
 */

#ifndef INCLUDE_DRAW_H_
#define INCLUDE_DRAW_H_

#include <stdbool.h>
#include <stdlib.h>

#include "graphics.h"

#define draw_pixel(ctx, x, y) ctx->draw_pixel(ctx, x, y)
void draw_from_last_pixel(graphics_ctx const * const ctx, x_coord const x, y_coord const y);

void draw_pixels		(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, x_coord const x1, y_coord const y1);
void draw_line			(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, x_coord const x1, y_coord const y1);
void draw_line_bresenham(graphics_ctx const * const ctx, x_coord x0, y_coord y0, x_coord const x1, y_coord const y1);

void draw_circle	(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, int const radius, draw_line_f* const f);
void draw_ellipse	(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, int const radius, int xmult, int xdiv, draw_line_f* const f);
void draw_box		(graphics_ctx const * const ctx, x_coord const x0, y_coord const y0, int width, int height, draw_line_f* const f);

#define draw_dot(ctx, dot) ctx->draw_pixel(ctx, dot.x, got.y)
#define draw_dots(ctx, dots, size) draw_lines(ctx, dots, size, ctx->put_pixel)
void draw_lines		(graphics_ctx const * const ctx, xy_coord const * const dots, size_t const size, put_pixel_f* const f);
void draw_polygon	(graphics_ctx const * const ctx, xy_coord const * const dots, size_t const size, put_pixel_f* const f);

#endif /* INCLUDE_DRAW_H_ */
