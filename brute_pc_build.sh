#!/usr/bin/env bash

CC="gcc"
CCO=""
CCINC="-I ./include"



function build_test_addr()
{
	mkdir -p ./bin/test/pc
	$CC $CCO $CCINC -o ./bin/test/pc/testa_addr ./src/test/pc/testa_addr.c ./src/lib/graphics/addr.c 
}

function build_all()
{
	build_test_addr
}

build_all